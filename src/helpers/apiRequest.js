import axios from 'axios';

const URL = 'https://mivotbackend.herokuapp.com/api';
// const URL = 'http://localhost:5500/api';

const endpoints = {
  tokens: `${URL}/tokens`,
  votations: `${URL}/votations`,
};

export const startVotations = () => {
  return axios.post(endpoints.votations, {});
};

export const getResults = () => {
  return axios.get(endpoints.votations);
};
