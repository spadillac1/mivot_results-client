import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyAmwd6U9IIy5Qb7OenQ_LAFjJJXy3n82Pc',
  authDomain: 'mivot-306fe.firebaseapp.com',
  databaseURL: 'https://mivot-306fe.firebaseio.com',
  projectId: 'mivot-306fe',
  storageBucket: 'mivot-306fe.appspot.com',
  messagingSenderId: '585013058583',
  appId: '1:585013058583:web:890dc90deb6a38a2db9052',
};

export const initializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);
}

export const signOut = () => {
  return firebase.auth().signOut();
}

export const signInWithLogin= (email, password) => {
  return firebase
   .auth()
   .signInWithEmailAndPassword(email, password)
   .then((response) => response);
}