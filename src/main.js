import Vue from 'vue';
import App from './App.vue';
import Buefy from 'buefy';
import { initializeFirebase } from './firebase';

Vue.use(Buefy);
Vue.config.productionTip = false;
initializeFirebase();

new Vue({
  render: (h) => h(App),
}).$mount('#app');
